[![Yii UI](https://avatars1.githubusercontent.com/u/22790740?s=60)](https://www.yii-ui.com/) Helper views for some other yii-ui modules.
================================================

[![Latest Stable Version](https://poser.pugx.org/yii-ui/yii2-base-views/version)](https://packagist.org/packages/yii-ui/yii2-base-views)
[![Total Downloads](https://poser.pugx.org/yii-ui/yii2-base-views/downloads)](https://packagist.org/packages/yii-ui/yii2-base-views)
[![Yii2](https://img.shields.io/badge/Powered_by-Yii_Framework-green.svg?style=flat)](http://www.yiiframework.com/)
[![License](https://poser.pugx.org/yii-ui/yii2-base-views/license)](https://packagist.org/packages/yii-ui/yii2-base-views)


Installation
------------

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run
```
php composer.phar require yii-ui/yii2-base-views
```
or add
```
"yii-ui/yii2-base-views": "~1.0.0"
```
to the require section of your `composer.json` file.

Usage
-----

```php
TODO
```

More [Examples](https://www.yii-ui.com/yii2-base-views) will be added soon at https://www.yii-ui.com/yii2-base-views.

Documentation
------------

[Documentation](https://www.yii-ui.com/yii2-base-views) will be added soon at https://www.yii-ui.com/yii2-base-views.

License
-------

**yii2-base-views** is released under the MIT License. See the [LICENSE.md](LICENSE.md) for details.
