<?php
use yii\helpers\Html;

if (isset($buttons) && $buttons instanceof \Closure) {
    $buttons = call_user_func($buttons, $model??null, $form??null, $this);
}

$cardOptions = $cardOptions??[];
Html::addCssClass($cardOptions, ['card' => 'card']);
echo Html::beginTag('div', $cardOptions);

if (isset($title) || isset($content)) {
    $cardContentOptions = $cardContentOptions??[];
    Html::addCssClass($cardContentOptions, 'card-content');
    echo Html::beginTag('div', $cardContentOptions);

    if (isset($title)) {
        echo Html::beginTag('div', ['class' => 'card-title']);

        if ((!isset($buttonsInTitle) || $buttonsInTitle) && isset($buttons) && count($buttons) > 0) {
            echo Html::beginTag('div', ['class' => 'btn-group']);

            foreach ($buttons as $button) {
                $options = $button['options']??[];

                if (($button['type']??null) === null) {
                    $button['type'] = 'link';
                }

                Html::addCssClass($options, [
                    'btn-floating',
                    'waves-effect',
                    'waves-light'
                ]);

                if ((!isset($buttonsInTitleTooltipped) || $buttonsInTitleTooltipped)) {
                    Html::addCssClass($options, [
                        'tooltipped'
                    ]);

                    $options['data-position'] = 'bottom';
                    $options['data-tooltip'] = $button['label'].(isset($options['data-tooltip'])?'<br><br>'.$options['data-tooltip']:'');
                }

                switch ($button['type']) {
                    case 'button':
                        if (isset($button['url'])) {
                            $options['data-url'] = $button['url'];
                        }

                        $buttonHtml = Html::button('<i class="'.$button['icon'].'"></i>', $options);
                        break;
                    case 'reset':
                        if (isset($button['form'])) {
                            $options['form'] = $button['form'];
                        }

                        $buttonHtml = Html::resetButton('<i class="'.$button['icon'].'"></i>', $options);
                        break;
                    case 'submit':
                        if (isset($button['form'])) {
                            $options['form'] = $button['form'];
                        }

                        $buttonHtml = Html::submitButton('<i class="'.$button['icon'].'"></i>', $options);
                        break;
                    default:
                        $buttonHtml = Html::a('<i class="'.$button['icon'].'"></i>', $button['url'], $options);
                        break;
                }

                echo $buttonHtml;
            }

            echo Html::endTag('div');
        }

        if (isset($icon)) {
            echo '<i class="mdi mdi-'.$icon.' left"></i>';
        }

        echo Html::tag('span', $title);

        echo Html::endTag('div');
    }

    if (isset($content)) {
        echo $content;
    }

    echo Html::endTag('div');
}

if (isset($buttons) && count($buttons) > 0) {
    $cardActionOptions = $cardActionOptions??[];
    Html::addCssClass($cardActionOptions, ['card-action' => 'card-action']);

    echo Html::beginTag('div', $cardActionOptions);
    echo Html::beginTag('div', ['class' => 'row']);

    foreach ($buttons as $button) {
        $options = $button['options']??[];

        if (($button['type']??null) === null) {
            $button['type'] = 'link';
        }

        Html::addCssClass($options, [
            'btn',
            'full-width',
            'waves-effect',
            'waves-light'
        ]);

        switch ($button['type']) {
            case 'button':
                if (isset($button['url'])) {
                    $options['data-url'] = $button['url'];
                }

                $buttonHtml = Html::button('<i class="'.$button['icon'].' left"></i>'.$button['label'], $options);
                break;
            case 'reset':
                if (isset($button['form'])) {
                    $options['form'] = $button['form'];
                }

                $buttonHtml = Html::resetButton('<i class="'.$button['icon'].' left"></i>'.$button['label'], $options);
                break;
            case 'submit':
                if (isset($button['form'])) {
                    $options['form'] = $button['form'];
                }

                $buttonHtml = Html::submitButton('<i class="'.$button['icon'].' left"></i>'.$button['label'], $options);
                break;
            default:
                $buttonHtml = Html::a('<i class="'.$button['icon'].' left"></i>'.$button['label'], $button['url'], $options);
                break;
        }

        echo Html::tag('div', $buttonHtml, [
            'class' => [
                'col',
                'm'.round(12 / count($buttons)),
                's12'
            ]
        ]);
    }

    echo Html::endTag('div');
    echo Html::endTag('div');
}

echo Html::endTag('div');
