<?php foreach ($rows as $row) :?>
    <div class="row">
        <?php foreach ($row as $column) :?>
            <?=\yii\helpers\Html::beginTag('div', $column['options']);?>
                <?=$column['content'];?>
            <?=\yii\helpers\Html::endTag('div');?>
        <?php endforeach;?>
    </div>
<?php endforeach;?>