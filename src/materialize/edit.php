<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$formOptions = array_merge(['id' => strtolower($model->formName()).'-form'], $formOptions??[]);

echo Html::beginTag('div', ['id' => $formOptions['id'].'-wrapper']);

if (!isset($formClass)) {
    $formClass = ActiveForm::class;
}

$form = $formClass::begin($formOptions);

if (isset($buttons) && $buttons instanceof \Closure) {
    $buttons = call_user_func($buttons, $model, $form, $this);
}

if ((!isset($isWidget) || !$isWidget) && isset(Yii::$app->fafcms) && isset($buttons) && !empty($buttons)) {
    Yii::$app->fafcms->actionButtons = $buttons;
}

if (isset($tabs)) {
    $id = null;

    if (!isset($tabContainerOptions)) {
        $tabContainerOptions = [];
    }

    if (!isset($tabWrapperOptions)) {
        $tabWrapperOptions = [];
    }

    Html::addCssClass($tabWrapperOptions, 'tab-wrapper');

    if (!isset($isMainTabContainer) || $isMainTabContainer) {
        Html::addCssClass($tabContainerOptions, 'main-tabs');
        $id = 'main-form-tab';
    }

    echo Html::tag('div', $this->render('@yiiui/yii2baseviews/materialize/tabs', [
        'tabs' => $tabs,
        'form' => $form,
        'model' => $model,
        'id' => $id,
        'tabContainerOptions' => $tabContainerOptions
    ]), $tabWrapperOptions);
} elseif (isset($content)) {
    ?>
    <div class="row">
        <div class="col s12">
            <div class="content-wrapper"><?= call_user_func($content, $model, $form, $this) ?></div>
        </div>
    </div>
    <?php
}

if (isset($buttons) && ($buttonCard??true)) {
    $buttonCardOptions = array_merge(['buttons' => $buttons], $buttonCardOptions??[]);

    echo $this->render('rows', [
        'rows' => [[[
            'options' => ['class' => 'col s12'],
            'content' => $this->render('card', $buttonCardOptions),
        ]]],
    ]);
}

$formClass::end();
echo Html::endTag('div');
