<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$tabs = call_user_func($tabs, $model, (isset($form)?$form:$this), $this);

if (!isset($id) || $id === null) {
    $id = Yii::$app->security->generateRandomString(10);
}

if (!isset($tabContainerOptions)) {
    $tabContainerOptions = [];
}

if (!isset($tabContainerOptions['id'])) {
    $tabContainerOptions['id'] = $id;
}

Html::addCssClass($tabContainerOptions, 'tabs tabs-fixed-width z-depth-1');

echo Html::beginTag('ul', $tabContainerOptions);

foreach ($tabs as $i => $tab) {
    echo Html::tag('li', Html::a($tab['label'], '#'.(isset($tab['id'])?$tab['id']:$id.'-'.$i), ['class' => ($tab['active']??false)?'active':'']), ['class' => 'tab']);
}

echo Html::endTag('ul');
?>
<div class="row">
    <?php foreach ($tabs as $i => $tab): ?>
        <?= Html::tag('div', $tab['content'], [
            'id' => (isset($tab['id'])?$tab['id']:$id.'-'.$i),
            'class' => 'tab col s12',
            'data-tab-instance' => $id
        ])?>
    <?php endforeach;?>
</div>