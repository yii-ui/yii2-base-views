<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

if (isset($buttons) && $buttons instanceof \Closure) {
    $buttons = call_user_func($buttons, $model??null, $this);
}

if ((!isset($isWidget) || !$isWidget) && isset(Yii::$app->fafcms) && isset($buttons) && !empty($buttons)) {
    Yii::$app->fafcms->actionButtons = $buttons;
}

if (!isset($tabContainerOptions)) {
    $tabContainerOptions = [];
}

Html::addCssClass($tabContainerOptions, 'main-tabs');

if (isset($tabs)) {
    echo $this->render('@yiiui/yii2baseviews/materialize/tabs', [
        'tabs' => $tabs,
        'model' => $model??null,
        'id' => 'main-form-tab',
        'tabContainerOptions' => $tabContainerOptions
    ]);
} elseif (isset($content)) {
    ?>
    <div class="row">
        <div class="col s12">
            <div class="content-wrapper"><?= call_user_func($content, $model??null, $this) ?></div>
        </div>
    </div>
    <?php
}

if (isset($buttons) && ($buttonCard??true)) {
    $buttonCardOptions = array_merge(['buttons' => $buttons], $buttonCardOptions??[]);

    echo $this->render('rows', [
        'rows' => [[[
            'options' => ['class' => 'col s12'],
            'content' => $this->render('card', $buttonCardOptions),
        ]]],
    ]);
}
