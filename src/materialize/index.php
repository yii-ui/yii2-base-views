<?php
use yiiui\yii2advancedgridview\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

if (!isset($sort) || $sort) {
    ArrayHelper::multisort($columns, 'sort');
}

if (isset($buttons) && $buttons instanceof \Closure) {
    $buttons = call_user_func($buttons, $dataProvider, $filterModel, $this);
}

if ((!isset($isWidget) || !$isWidget) && isset(Yii::$app->fafcms) && isset($buttons) && !empty($buttons)) {
    Yii::$app->fafcms->actionButtons = $buttons;
}

if (!isset($gridViewClass)) {
    $gridViewClass = GridView::class;
}

$gridViewOptions = array_merge([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => $columns
], $gridViewOptions??[]);

Pjax::begin();

echo $this->render('@yiiui/yii2baseviews/materialize/rows', [
    'rows' => [[[
        'options' => ['class' => 'col s12'],
        'content' => $this->render('@yiiui/yii2baseviews/materialize/card', [
            'title' => $title??null,
            'icon' => $icon??null,
            'content' => $gridViewClass::widget($gridViewOptions),
            'buttons' => $buttons??[]
        ]),
    ]]],
]);

Pjax::end();
