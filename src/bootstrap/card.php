<?php
use yii\bootstrap\Html;

if (isset($buttons) && $buttons instanceof \Closure) {
    $buttons = call_user_func($buttons, $model??null, $form??null, $this);
}

$cardOptions = $cardOptions??[];
Html::addCssClass($cardOptions, 'panel panel-default');
echo Html::beginTag('div', $cardOptions);

if (isset($title) || isset($content)) {
    if (isset($title)) {
        echo Html::tag('div', (isset($icon)?'<i class="'.$icon.' left"></i>':'').$title, ['class' => 'panel-heading']);
    }

    if (isset($content)) {
        $cardContentOptions = $cardContentOptions??[];
        Html::addCssClass($cardContentOptions, 'panel-body');
        echo Html::tag('div', $content, $cardContentOptions);
    }
}

if (isset($buttons) && count($buttons) > 0) {
    $cardActionOptions = $cardActionOptions??[];
    Html::addCssClass($cardActionOptions, 'panel-footer');

    echo Html::beginTag('div', $cardActionOptions);
    echo Html::beginTag('div', ['class' => 'row']);

    foreach ($buttons as $button) {
        $options = $button['options']??[];

        if (($button['type']??null) === null) {
            $button['type'] = 'link';
        }

        Html::addCssClass($options, [
            'btn',
            'full-width',
            'btn-default'
        ]);

        switch ($button['type']) {
            case 'button':
                $options['data-url'] = $button['url'];
                $buttonHtml = Html::button('<i class="'.$button['icon'].' left"></i>'.$button['label'], $options);
                break;
            case 'submit':
                $options['form'] = $button['form'];
                $buttonHtml = Html::submitButton('<i class="'.$button['icon'].' left"></i>'.$button['label'], $options);
                break;
            default:
                $buttonHtml = Html::a('<i class="'.$button['icon'].' left"></i>'.$button['label'], $button['url'], $options);
                break;
        }

        echo Html::tag('div', $buttonHtml, [
            'class' => [
                'col-md-'.round(12 / count($buttons)),
                'col-xs-12'
            ]
        ]);
    }

    echo Html::endTag('div');
    echo Html::endTag('div');
}

echo Html::endTag('div');
