<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$formOptions = array_merge(['id' => 'main-form'], $formOptions??[]);

echo Html::beginTag('div', ['id' => $formOptions['id'].'-wrapper']);

if (!isset($formClass)) {
    $formClass = ActiveForm::class;
}

$form = $formClass::begin($formOptions);

if (isset($buttons) && $buttons instanceof \Closure) {
    $buttons = call_user_func($buttons, $model, $form, $this);
}

if ((!isset($isWidget) || !$isWidget) && isset(Yii::$app->fafcms) && isset($buttons) && !empty($buttons)) {
    Yii::$app->fafcms->actionButtons = $buttons;
}

if (!isset($tabContainerOptions)) {
    $tabContainerOptions = [];
}

Html::addCssClass($tabContainerOptions, 'main-tabs');

if (isset($tabs)) {
    echo $this->render('@yiiui/yii2baseviews/materialize/tabs', [
        'tabs' => $tabs,
        'form' => $form,
        'model' => $model,
        'id' => 'main-form-tab',
        'tabContainerOptions' => $tabContainerOptions
    ]);
} elseif (isset($content)) {
    ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="content-wrapper"><?= call_user_func($content, $model, $form, $this) ?></div>
        </div>
    </div>
    <?php
}

if (isset($buttons) && ($buttonCard??true)) {
    $buttonCardOptions = array_merge(['buttons' => $buttons], $buttonCardOptions??[]);

    echo $this->render('rows', [
        'rows' => [[[
            'options' => ['class' => 'col-xs-12'],
            'content' => $this->render('card', $buttonCardOptions),
        ]]],
    ]);
}

$formClass::end();
echo Html::endTag('div');
