<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$tabs = call_user_func($tabs, $model, (isset($form)?$form:$this), $this);

if (!isset($id)) {
    $id = Yii::$app->security->generateRandomString(10);
}

if (!isset($tabContainerOptions)) {
    $tabContainerOptions = [];
}

if (!isset($tabContainerOptions['id'])) {
    $tabContainerOptions['id'] = $id;
}

Html::addCssClass($tabContainerOptions, 'nav nav-tabs');

$tabContainerOptions['role'] = 'tablist';

echo Html::beginTag('div');
echo Html::beginTag('ul', $tabContainerOptions);

foreach ($tabs as $i => $tab) {
    echo Html::tag('li', Html::a($tab['label'], '#'.(isset($tab['id'])?$tab['id']:$id.'-'.$i), ['aria-controls' => 'profile', 'role' => 'tab', 'data-toggle' => 'tab']), ['class' => ['tab', ($tab['active']??false)?'active':''], 'role' => 'presentation']);
}

echo Html::endTag('ul');
?>
<div class="tab-content">
    <?php foreach ($tabs as $i => $tab): ?>
        <?= Html::tag('div', $tab['content'], [
            'id' => (isset($tab['id'])?$tab['id']:$id.'-'.$i),
            'class' => 'col-xs-12 tab-pane '.(($tab['active']??false)?'active':''),
            'role' => 'tabpanel',
            'data-tab-instance' => $id
        ])?>
    <?php endforeach;?>
</div>
<?= Html::endTag('div');?>